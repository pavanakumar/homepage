+++
# Date this page was created.
date = "2016-04-27"

# Project title.
title = "OpenFUSE library and tools"

# Project summary to display on homepage.
summary = "Framework for Unstructred Mesh Solvers: Mesh data-base, partitioner, and load balancer"

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = "fuse_logo.png"

# Optional image to display on project detail page (relative to `static/img/` folder).
image = ""

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["C++"]

# Optional external URL for project (replaces project detail page).
external_link = "http://pavanakumar.github.io/OpenFUSE/"

# Does the project detail page use math formatting?
math = false

+++

