+++
# Date this page was created.
date = "2016-04-27"

# Project title.
title = "Element-free solver and tools"

# Project summary to display on homepage.
summary = "Library and tools for element-free or meshless solvers"

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = "gf_logo.png"

# Optional image to display on project detail page (relative to `static/img/` folder).
image = ""

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["FORTRAN"]

# Optional external URL for project (replaces project detail page).
external_link = "https://github.com/pavanakumar/elementFree"

# Does the project detail page use math formatting?
math = false

+++

