+++
# About/Biography widget.
widget = "about"
active = true
date = "2016-04-20T00:00:00"

# Order that this section will appear in.
weight = 5

# List your academic interests.
[interests]
  interests = [
    "Uncertanity quantification",
    "Adjoint methods",
    "Aero-acoustic design"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "PhD in Aerospace Engineering"
  institution = "Queen Mary University of London"
  year = 2019

[[education.courses]]
  course = "MS in Aerospace Engineering"
  institution = "Pennsylvania State University"
  year = 2010

[[education.courses]]
  course = "B.Tech in Aerospace Engineering"
  institution = "Madras Institute of Technology"
  year = 2005
 
+++

# Biography
I am a senior researcher at ALGO-COOP team, CERFACS, Toulouse, France.
My interests are in the area of uncertainty quantification, optimisation,
and computational fluid dynamics. At CERFACS I develop and maintain the
TreePart/Adapt framework for large scale online unstructured mesh
partitioning and adaptation. We recently achieved scalable online 
partitioning up to 132k MPI ranks fully exploiting the hardware
hierarchy of the parallel machines (node/sockets/cores).

Am also the developer and maintainer of the Turbomachinery CFD/Adjoint
solver (STAMPS) along with Dr. Jens-Dominik Mueller at Queen Mary University
of London. STAMPS is one of the first Turbomachinery RANS solvers based on
Newton-Krylov method using exact Jacobian obtained using source-transformed
Algorithmic differentation.


Feel free to explore my open source projects and the most starred one on 
GitHub the [compressibleFoam](https://pavanakumar.github.io/compressibleFoam/)
solver (an excellent CFD teaching aid) and my blog posts.
