+++

abstract = "In the present study, two types of least-squares approximation, namely, Taylor series extrapolation and polynomial interpolation, has been implemented in a parallel inviscid compressible computational fluid dynamics code based on Finite Point Method. The flow over standard Onera M6 wing at transonic Mach number has been simulated using the two approximations. Numerical stability issues with Taylor series approximation has been reported, while the polynomial least-squares approximation is show to be robust and stable. Explicit pseudo-time integration has been used to drive the flow to steady state. The possible reasons for the robustness in the polynomial least-squares has been discussed and demonstrated through numerical experiment."

abstract_short = ""

authors = ["M Pavanakumar"]
date = "2012-08-01"
image = ""
image_preview = ""
math = true
publication = "Proceedings of Aeronautical Society of India, 14th Annual CFD Symposium"
publication_short = "In *AESI* 2012 conference"
selected = false
title = "Study on Robustness of least-squares approximation in finite point method for transonic flows"
url_code = "#"
url_dataset = "#"
url_pdf = ""
url_project = "#"
url_slides = "#"
url_video = "#"

[[url_custom]]
name = "Custom Link"
url = ""

+++



