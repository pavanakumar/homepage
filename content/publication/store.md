+++

abstract = "Grid-free Euler solver using Entropy (q) variables based Least Squares Kinetic Upwind Method (q-LSKUM) has been successfully applied to a store-separation dynamics problem. Grid-free methods require just a cloud of points distributed over the computational domain and a set of neighbours (called connectivity) around each point. In the present study, the clouds of points are generated around wing and store separately and overlapped to get a cloud of points (called chimera cloud) around the configuration. The store cloud is moved relative to the wing cloud and the connectivity is updated at every time step. Dynamic blanking and connectivity generation for the chimera cloud of points are carried out using the background octtree search method. The aerodynamic forces and moments are obtained using the q-LSKUM Euler code. Then, the forces and moments have been used to solve the six degrees of freedom (6DOF) equations for obtaining the new position of the store. The procedure has been used in the simulation of a store separating from the wing-pylon configuration. The trajectory of the store and pressure distributions on the store surface are compared with the experimental data."
abstract_short = "Grid-free Euler solver using Entropy (q) variables based Least Squares Kinetic Upwind Method (q-LSKUM) has been successfully applied to a store-separation dynamics problem."
authors = ["G Harish", "M Pavanakumar", "K Anandhanarayanan"]
date = "2006-06-01"
image = ""
image_preview = ""
math = true
publication = "AIAA Applied Aerodynamics Conference"
publication_short = "In *AIAA* Applied Aerodynamics Conference"
selected = false
title = "Store Separation Dynamics using Grid-free Euler Solver"
url_code = "#"
url_dataset = "#"
url_pdf = "http://arc.aiaa.org/doi/pdf/10.2514/6.2006-3650"
url_project = "#"
url_slides = "#"
url_video = "#"

[[url_custom]]
name = "Custom Link"
url = "http://arc.aiaa.org/doi/pdf/10.2514/6.2006-3650"

+++

