+++

abstract = "In this work, we present a least-squares vertex-based mesh quality metric for optimised mesh smoothing, which is inspired from meshless methods. The proposed mesh quality metric requires only point cloud information and does not require mesh topology information. In addition, it is scale, rotational, and translation invariant making it suitable for highly stretched boundary layer type meshes. The proposed quality metric is implemented in the Mesquite mesh optimisation framework. The mesh optimisation is combined with a robust mesh deformation algorithm based on inverse distance weighting for aerodynamic shape optimisation problem. Mesh untangling can be performed using the same mesh optimisation framework by choosing an appropriate quality metric. We show that combining mesh deformation with optimized mesh smoothing can increase the number of design iterations without requiring re-meshing and allows more room for large shape deformation."

abstract_short = "We present a least-squares vertex-based mesh quality metric for optimised mesh smoothing, which is inspired from meshless methods."

authors = ["M Pavanakumar", "J D Mueller"]
date = "2016-11-01"
image = ""
image_preview = ""
math = true
publication = "NOED conference"
publication_short = "In *NOED* conference"
selected = false
title = "A meshless optimised mesh-smoothing framework"
url_code = "#"
url_dataset = "#"
url_pdf = "http://aboutflow.sems.qmul.ac.uk/content/events/munich2016/schedule/files/NOED2016_paper_46(1).pdf"
url_project = "#"
url_slides = "#"
url_video = "#"

[[url_custom]]
name = "Custom Link"
url = "http://aboutflow.sems.qmul.ac.uk/content/events/munich2016/schedule/files/NOED2016_paper_46(1).pdf"

+++

