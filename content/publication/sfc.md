+++

abstract = "Graph based algorithms are widely used for unstructured mesh partitioning. They give rise to non-contiguous partitions optimized for minimum communication between partitions. Space-filling curves (SFC) on the other hand give better balanced partitions having higher communication cost than graphs. In this work, we show that SFC based algorithms are competitive to graph based methods, especially for I/O and compute intensive scientific applications. The basic idea used in this work is the SFC reordered storage in Hierarchical Data Format (HDF), which simplifies mesh partitioning, improves I/O bandwidth and at the same time provide cache friendly memory access. The ordering enables us to read the mesh entities as contiguous chunks using parallel I/O, which precludes the use of elaborate partitioning tools. We show comparisons of various partition quality metrics between partitions obtained from SFC and graph based methods for four types of unstructured meshes. The results show that SFC outperforms graph based partitioning in all the quality metrics except the communication volume. Finally, we show that storing the mesh entities using SFC leads to better cache locality without additional local reordering effort."
abstract_short = "The basic idea used in this work is the SFC reordered storage in Hierarchical Data Format (HDF), which simplifies mesh partitioning, improves I/O bandwidth and at the same time provide cache friendly memory access."
authors = ["M Pavanakumar", "K N Kaushik"]
date = "2013-12-01"
image = ""
image_preview = ""
math = true
publication = " High Performance Computing (HiPC), 2013"
publication_short = "In *HIPC*"
selected = false
title = "Revisiting the space-filling curves for storage, reordering and partitioning mesh based data in scientific computing"
url_code = "#"
url_dataset = "#"
url_pdf = ""
url_project = "#"
url_slides = "#"
url_video = "#"

[[url_custom]]
name = "Custom Link"
url = ""

+++

