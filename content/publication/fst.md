+++

abstract = "In this work, we present a least-squares vertex-based mesh quality metric for optimised mesh smoothing, which is inspired from meshless methods. The proposed mesh quality metric requires only point cloud information and does not require mesh topology information. In addition, it is scale, rotational, and translation invariant making it suitable for highly stretched boundary layer type meshes. The proposed quality metric is implemented in the Mesquite mesh optimisation framework. The mesh optimisation is combined with a robust mesh deformation algorithm based on inverse distance weighting for aerodynamic shape optimisation problem. Mesh untangling can be performed using the same mesh optimisation framework by choosing an appropriate quality metric. We show that combining mesh deformation with optimized mesh smoothing can increase the number of design iterations without requiring re-meshing and allows more room for large shape deformation."

abstract_short = ""

authors = [ "T K Sengupta", "D Das", "P Mohanamuraly", "V K Suman", "A Biswas"]
date = "2009-09-01"
image = ""
image_preview = ""
math = true
publication = "International Journal of Emerging Multidisciplinary Fluid Sciences"
publication_short = "In *IJEMFS* journal"
selected = false
title = "Modeling free-stream turbulence based on wind tunnel and flight data for instability studies"
url_code = "#"
url_dataset = "#"
url_pdf = "https://triggered.clockss.org/ServeContent?url=http://multi-science.atypon.com%2Fdoi%2Fpdf%2F10.1260%2F175683109789686718"
url_project = "#"
url_slides = "#"
url_video = "#"

[[url_custom]]
name = "Custom Link"
url = "https://triggered.clockss.org/ServeContent?url=http://multi-science.atypon.com%2Fdoi%2Fpdf%2F10.1260%2F175683109789686718"

+++

