+++
title = "Wrapping Fortran Derived Types : Part 2"
date = 2019-05-24T16:08:09+02:00
draft = false

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []
categories = []

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
# Use `caption` to display an image caption.
#   Markdown linking is allowed, e.g. `caption = "[Image credit](http://example.org)"`.
# Set `preview` to `false` to disable the thumbnail in listings.
[header]
image = ""
caption = ""
preview = true

+++

In a previous post wrapping user-defined types in Fortran into C was explained using the opaque object hack. The hack was improved by adding portable size detection and direct access to array memory using pointer handles. For compilers that do not yet support ``STORAGE_SIZE`` intrinsic function an alternative route is suggested in this post. Again the approach is based on the one by *Pletzer et. al., ''Exposing Fortran derived types to C and other languages'', 2008* but modified and improved to increase portability and safety.

