+++
title = "Wrapping Fortran Derived Types : Part 1"
date = 2019-05-23T00:53:26+02:00
draft = false

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []
categories = []

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
# Use `caption` to display an image caption.
#   Markdown linking is allowed, e.g. `caption = "[Image credit](http://example.org)"`.
# Set `preview` to `false` to disable the thumbnail in listings.
[header]
image = ""
caption = ""
preview = true

+++

Abstract
---------

Fortran C/C++/Python interoperability is an important consideration while designing scientific HPC codes. Fortran to a large extent still dominates in terms of lines of code in scientific software due to its legacy, reliable mathematical libraries and many years of research into the design of its compilers. Therefore it becomes mandatory to support an array of codes and libraries developed in mixed languages say Fortran, C and C++. It is counter productive to rewrite the codes and revalidate just for the sake of using a single monolithic language.

Some shortcomings of `iso_c_binding`
------------------------------------

The Fortran 2003 standard introduced the ``iso_c_binding`` module to formalise known C interoperability hacks into Fortran language. It still leaves out a dark area, namely wrapping user defined types when the type has allocatable or pointer attributes. A simple example is shown below:

{{< highlight Fortran "linenos=inline" >}}
type :: my_type
  real(c_double), pointer :: my_data(:)
end type my_type
{{< / highlight >}}

Note that one could also include an allocatable array inside the user defined type such as:

{{< highlight Fortran "linenos=inline" >}}
type :: my_type
  real(c_double), allocatable :: my_data(:)
end type my_type
{{< / highlight >}}

The user is advised against using this approach since this is not flexible enough to expose the data outside of Fortran because a pointer variable cannot be made to point to this target array ``my_data`` if it was declared ``allocatable``. For the very reason the following piece of code is invalid:

{{< highlight Fortran "linenos=inline" >}}
type :: my_type
  real(c_double), allocatable, target :: my_data(:)
end type my_type
{{< / highlight >}}

When compiled in `gfortran` one gets following error:

{{< highlight  "linenos=inline" >}}
     real(c_double), allocatable, target :: my_data(:)
                                   1
Error: Attribute at (1) is not allowed in a TYPE definition
{{< / highlight >}}

Another approach one would be tempted to take is to use
the ``bind(C)`` technique from ``iso_c_binding`` as shown below:

{{< highlight Fortran "linenos=inline" >}}
type, bind(C) :: my_type
  real(c_double), allocatable, target :: my_data(:)
end type my_type
{{< / highlight >}}

But this results in a similar compiler error as the ``target`` approach:

{{< highlight  "linenos=inline" >}}
   type, bind(C) :: my_type
                          2
     real(c_double), pointer :: my_data(:)
                                         1
Error: Component 'my_data' at (1) cannot have the POINTER attribute because it is a member of the BIND(C) derived type 'my_type' at (2)
{{< / highlight >}}

----------------------------------------

**Technique 1**: Opaque object handle hack
-------------------------------------------
A famous trick employed by programmers to wrap user defined Fortran types into C is the use of scratch space handles or opaque objects. Simply put the C side allocates scratch space in memory (adequate enough to store the Fortran type) and pass that as a handle to Fortran. This handle can then be safely stored in the C side and passed around to Fortran functions to do computation. This has been give in detail in the work of *Pletzer et. al., ''Exposing Fortran derived types to C and other languages'', comp. sci. eng. 2008*. The object storing the scratch memory is called the **opaque object** to the Fortran type.

The opaque object technique applied to our simple problem is shown below:

{{< highlight Fortran "linenos=inline" >}}
! @file test.f90
! Fortran side code
module typedef
  use iso_c_binding
  type :: my_type
    real(c_double), pointer :: my_data(:)
  end type my_type
end module typedef

subroutine c_opaque_alloc(c_obj, n)
  use typedef
  implicit none
  integer(c_int), intent(in) :: n
  type(my_type) :: c_obj
  allocate(c_obj%my_data(n))
end subroutine c_opaque_alloc

subroutine c_opaque_free(c_obj)
  use typedef
  implicit none
  type(my_type) :: c_obj
  deallocate(c_obj%my_data)
end subroutine c_opaque_free

{{< / highlight >}}

In the C side the following code is used to allocate the user defined type:

{{< highlight c "linenos=inline" >}}
/** @file test.c
  * C side code
  */
#define FC_GLOBAL_(name,NAME) name##_
#define OPAQUE_ALLOC FC_GLOBAL_(c_opaque_alloc, C_OPAQUE_ALLOC)
#define OPAQUE_FREE FC_GLOBAL_(c_opaque_free, C_OPAQUE_FREE)

#define OPAQUE_STORAGE_SIZE 64

void OPAQUE_ALLOC(char *c_obj, int *n);
void OPAQUE_FREE(char *c_obj);

int main(int nargs, char *args[] ) {
  char c_obj[OPAQUE_STORAGE_SIZE];
  int n = 100;
  OPAQUE_ALLOC(c_obj, &n);
  OPAQUE_FREE(c_obj);
  return 0;
}
{{< / highlight >}}

The value of the `OPAQUE_STORAGE_SIZE` is an ambiguous one and varies from one compiler to another. Pletzer et. al. gives a long list of pointer array sizes used by various compilers based on their supported processor architecture. But the author does not give a reliable method to determine this size using existing Fortran features. This is one of the main contributions of this work where we provide a reliable method to obtain this size. Fortran 2008 standard defines a new inquiry function called ``STORAGE_SIZE``. As per the standards ``STORAGE_SIZE(A, [KIND])`` returns a scalar integer with the kind type parameter specified by ``KIND`` (or default integer type if ``KIND`` is missing). The result value is the size expressed in bits for an element of an array that has the dynamic type and type parameters of ``A``. Note that a dummy type ``dummy_type`` object was created in order to obtain the internal storage size in bits of this data structure. The routine would throw an error if one passed simple the ``my_type``. Currently there does not seem to be a better way to avoid this dummy type creation.

{{< highlight Fortran "linenos=inline" >}}
subroutine c_storage_size( my_size )
  use typedef
  implicit none
  type(my_type) :: dummy_obj
  integer(c_int) :: my_size
  my_size = STORAGE_SIZE(dummy_obj, c_int)
  my_size = my_size / 8 ! return size in bytes
end subroutine c_storage_size
{{< / highlight >}}

{{< highlight c "linenos=inline" >}}
#define FC_GLOBAL_(name,NAME) name##_
#define OPAQUE_ALLOC FC_GLOBAL_(c_opaque_alloc, C_OPAQUE_ALLOC)
#define OPAQUE_FREE FC_GLOBAL_(c_opaque_free, C_OPAQUE_FREE)
#define OPAQUE_SIZE FC_GLOBAL_(c_storage_size, C_STORAGE_SIZE)

void OPAQUE_ALLOC(char *c_obj, int *n);
void OPAQUE_FREE(char *c_obj);
void OPAQUE_SIZE(int *size);

int main(int nargs, char *args[] ) {
  char *c_obj;
  int my_size;
  OPAQUE_SIZE(&my_size);
  c_obj = (char *)malloc(my_size);
  int n = 100;
  OPAQUE_ALLOC(c_obj, &n);
  OPAQUE_FREE(c_obj);
  free(c_obj);
  return 0;
}
{{< / highlight >}}


In addition, the contents of the types can be indirectly exposed to C using Fortran call back functions (Getter/Setter type). While such an approach can be good for scalar values it is inefficient for array types because two copies of the arrays
must exist in memory (one in the C side and the other in
the Fortran side). In addition, the data copy/move from one to the other is redundant. An example of this approach is provided nevertheless:

{{< highlight Fortran "linenos=inline" >}}
subroutine c_opaque_array_copy(c_obj, n, vec)
  use typedef
  implicit none
  type(my_type) :: c_obj
  integer(c_int), intent(in) :: n
  real(c_double), intent(in) :: vec(n)
  allocate(c_obj%my_data(n))
  c_obj%my_data(1:n) = vec(1:n)
  write(*,*) c_obj%my_data
end subroutine c_opaque_array_copy
{{< / highlight >}}


{{< highlight c "linenos=inline" >}}
#include <stdlib.h>

#define FC_GLOBAL_(name,NAME) name##_
#define OPAQUE_ARRAY_COPY FC_GLOBAL_(c_opaque_array_copy, C_OPAQUE_ARRAY_COPY)
#define OPAQUE_FREE FC_GLOBAL_(c_opaque_free, C_OPAQUE_FREE)
#define OPAQUE_SIZE FC_GLOBAL_(c_storage_size, C_STORAGE_SIZE)
#define SIZE 100

void OPAQUE_FREE(char *c_obj);
void OPAQUE_SIZE(int *size);
void OPAQUE_ARRAY_COPY(char *c_obj, int *n, double *vec);

int main(int nargs, char *args[] ) {
  char *c_obj;
  int my_size, i, n=SIZE;
  double vec[SIZE];

  for(i=0; i<SIZE; ++i) vec[i] = -1.0;
  OPAQUE_SIZE(&my_size);
  c_obj = (char *)malloc(my_size);
  OPAQUE_ARRAY_COPY(c_obj, &n, vec);
  OPAQUE_FREE(c_obj);
  free(c_obj);
  return 0;
}
{{< / highlight >}}

One can improve upon this recipe and also return handles to the arrays/scalars inside the opaque handle to avoid redundant storage. C can then use these array handles to access the array data without any redundant copy/move.

{{< highlight Fortran "linenos=inline" >}}
subroutine c_opaque_array(c_obj, c_array)
  use typedef
  implicit none
  type(my_type) :: c_obj
  type(c_ptr), intent(out) :: c_array
  c_array = c_loc(c_obj%my_data(1))
end subroutine c_opaque_array
{{< /highlight  >}}

{{< highlight c "linenos=inline" >}}
#define FC_GLOBAL_(name,NAME) name##_
#define OPAQUE_ALLOC FC_GLOBAL_(c_opaque_alloc, C_OPAQUE_ALLOC)
#define OPAQUE_ARRAY FC_GLOBAL_(c_opaque_array, C_OPAQUE_ARRAY)
#define OPAQUE_FREE FC_GLOBAL_(c_opaque_free, C_OPAQUE_FREE)
#define OPAQUE_SIZE FC_GLOBAL_(c_storage_size, C_STORAGE_SIZE)
#define SIZE 100

void OPAQUE_ALLOC(char *c_obj, int *n);
void OPAQUE_FREE(char *c_obj);
void OPAQUE_SIZE(int *size);
void OPAQUE_ARRAY(char *c_obj, double **vec);

int main(int nargs, char *args[] ) {
  char *c_obj;
  int my_size, i, n=SIZE;
  double *vec;

  OPAQUE_SIZE(&my_size);
  c_obj = (char *)malloc(my_size);
  OPAQUE_ALLOC(c_obj, &n);
  OPAQUE_ARRAY(c_obj, &vec);
  for(i=0; i<SIZE; ++i)
    vec[i] = -1.0;
  OPAQUE_FREE(c_obj);
  free(c_obj);
  return 0;
}
{{< /highlight  >}}

While the ``STORAGE_SIZE`` primitive gave us a portable and safe solution to wrap user defined types, some older Fortran compiler might not support the Fortran 2008 standards yet. In such situations one should resort to an alternative approach using ``TRANSFER`` which will be presented in the second part of this work.

*Access to the source code with Makefiles to be uploaded soon to GitHub after testing against Intel, GCC, PGI, and LLVM compilers.*

